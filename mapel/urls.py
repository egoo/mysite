from django.conf.urls import url, include
from mapel import views
from me.models import mapel
from django.views.generic import ListView, DetailView

urlpatterns = [

	url(r'^data_mapel/', views.data_mapel, name='data_mapel'),
	url(r'^input/', views.input),
	url(r'^create_mapel', views.create_mapel),
	url(r'^edit/(?P<id>\d+)$', views.edit),
	url(r'^update/(?P<id>\d+)$', views.update),
	url(r'^delete/(?P<id>\d+)$', views.delete),
]
