# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template.response import TemplateResponse
from django.shortcuts import render, redirect
from me.models import mapel

# Create your views here.
def data_mapel(request):
	mapel_data = mapel.objects.all()
	return TemplateResponse(request, 'mapel/data_mapel.html', { "mapel_data": mapel_data })

def input(request):
	mapel_data = mapel.objects.all()
	return render(request, 'mapel/create.html')

def create_mapel(request):
	print request.POST
	mapel_data = mapel(mata_pelajaran=request.POST['mata_pelajaran'])
	mapel_data.save()
	return redirect('/mapel/data_mapel')

def edit(request, id):
	mapel_data = mapel.objects.get(id=id)
	return TemplateResponse(request, 'mapel/edit.html', { "mapel_data": mapel_data })

def update(request, id):
	mapel_data = mapel.objects.get(id=id)
	mapel_data.mata_pelajaran = request.POST['mata_pelajaran']
	mapel_data.save()
	return redirect('/mapel/data_mapel')

def delete(request, id):
	mapel_data = mapel.objects.get(id=id)
	mapel_data.delete()
	return redirect('/mapel/data_mapel')

	

	