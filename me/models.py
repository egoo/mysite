# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class data_siswa(models.Model):
	nis = models.CharField(max_length=200)
	nama = models.CharField(max_length=200)
	alamat = models.TextField()
	jekel= models.CharField(max_length=200)
 
class mapel(models.Model):

	mata_pelajaran = models.CharField(max_length=200)

class nilai(models.Model):
	siswa = models.ForeignKey(data_siswa, null=True,blank=True )
	mata_pelajaran = models.ForeignKey(mapel, null=True,blank=True)
	n_tugas = models.IntegerField(blank=True, null=True)
	n_mid = models.IntegerField(blank=True, null=True)
	n_uas = models.IntegerField(blank=True, null=True)

	class Meta:
		ordering = ['siswa', 'mata_pelajaran', 'n_tugas', 'n_mid', 'n_uas']

	
	

