# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-07 01:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('me', '0004_nilai'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nilai',
            name='mata_pelajaran',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='me.mapel'),
        ),
        migrations.AlterField(
            model_name='nilai',
            name='siswa',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='me.data_siswa'),
        ),
    ]
