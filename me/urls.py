from django.conf.urls import url, include
from . import views
from me.models import data_siswa
from django.views.generic import ListView, DetailView

urlpatterns = [
	url(r'^$', views.index, name="index" ),
	url(r'^create_siswa', views.create_siswa ),
	url(r'^create', views.create ),
	url(r'^data/', ListView.as_view(queryset=data_siswa.objects.all(),template_name="me/data_siswa.html")),
	url(r'^edit/(?P<id>\d+)$', views.edit),
	url(r'^update/(?P<id>\d+)$', views.update),
	url(r'^delete/(?P<id>\d+)$', views.delete),
]	