# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from me.models import data_siswa, mapel, nilai


admin.site.register(data_siswa)
admin.site.register(mapel)
admin.site.register(nilai)
# Register your models here.
