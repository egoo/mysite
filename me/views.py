# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from me.models import data_siswa


# Create your views here.
def index(request):
	return render(request, 'me/index.html')



def create_siswa(request):
	print request.POST
	siswa = data_siswa(nis=request.POST['nis'],nama=request.POST['nama'],alamat=request.POST['alamat'],jekel=request.POST['jekel'])
	siswa.save()
	return redirect('/data')
	
def data(request):
	return render(request, 'me/data_siswa.html')


def create(request):
	return render(request, 'me/create.html')


def edit(request, id):
	siswa = data_siswa.objects.get(id=id)
	context = {"siswa":siswa}
	return render(request, 'me/edit.html', context)

def update(request, id):
	siswa = data_siswa.objects.get(id=id)
	siswa.nis = request.POST['nis']
	siswa.nama = request.POST['nama']
	siswa.alamat = request.POST['alamat']
	siswa.jekel = request.POST['jekel']
	siswa.save()
	return redirect('/data')

def delete(request, id):
	siswa = data_siswa.objects.get(id=id)
	siswa.delete()
	return redirect('/data')






