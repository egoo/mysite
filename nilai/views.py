# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import Sum, F, Value

from django.template.response import TemplateResponse
from django.shortcuts import render, redirect
from me.models import nilai, mapel, data_siswa

# Create your views here.
def data_nilai(request):


	nilai_data = nilai.objects.all().annotate(n_jumlah = Sum(F('n_tugas')+F('n_mid') + F('n_uas')) / Value(3))

	return TemplateResponse(request, 'nilai/data_nilai.html', { "nilai_data": nilai_data })

def input(request):
	nilai_data = nilai.objects.all()
	mapel_data = mapel.objects.all()
	siswa_data = data_siswa.objects.all()
	return TemplateResponse(request, 'nilai/create.html', { "mapel_data": mapel_data, "siswa_data":siswa_data } )

def create_nilai(request):
	nilai_data = nilai();
	nilai_data.n_tugas = request.POST['n_tugas']
	nilai_data.n_mid = request.POST['n_mid']
	nilai_data.n_uas = request.POST['n_uas']
	nilai_data.mata_pelajaran_id = request.POST.get('mata_pelajaran_id')
	nilai_data.siswa_id = request.POST.get('siswa_id')
	nilai_data.save()
	return redirect('/nilai/data_nilai')

def edit(request, id):
	nilai_data = nilai.objects.get(id=id)
	mapel_data = mapel.objects.all()
	siswa_data = data_siswa.objects.all()
	return TemplateResponse(request, 'nilai/edit.html', { "nilai_data": nilai_data,  "mapel_data": mapel_data, "siswa_data":siswa_data })

def update(request, id):
	nilai_data = nilai.objects.get(id=id)
	nilai_data.mata_pelajaran_id = request.POST.get('mata_pelajaran_id')
	nilai_data.siswa_id = request.POST.get('siswa_id')
	nilai_data.n_tugas = request.POST['n_tugas']
	nilai_data.n_mid = request.POST['n_mid']
	nilai_data.n_uas = request.POST['n_uas']

	nilai_data.save()
	return redirect('/nilai/data_nilai')

def delete(request, id):
	nilai_data = nilai.objects.get(id=id)
	nilai_data.delete()
	return redirect('/nilai/data_nilai')