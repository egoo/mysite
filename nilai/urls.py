from django.conf.urls import url, include
from nilai import views
from me.models import nilai
from django.views.generic import ListView, DetailView

urlpatterns = [
		url(r'^data_nilai/', views.data_nilai, name='data_nilai'),
		url(r'^input/', views.input ),
		url(r'^create_nilai', views.create_nilai ),
		url(r'^edit/(?P<id>\d+)$', views.edit ),
		url(r'^update/(?P<id>\d+)$', views.update ),
		url(r'^delete/(?P<id>\d+)$', views.delete ),

]	